var notes = (function() {
    var list = [];

    return {
        add: function(note) {
            if (note &&  !/^\s*$/.test(note)) {
                var item = {timestamp: Date.now(), text: note};
                list.push(item);
                return true;
            }
            return false;
        },
        remove: function(index) {
            if(list.length >= index) {
                delete list[index];
                return true;
            }else {
                return false;
            }
        },
        count: function() {
            return list.length;
        },
        list: function() {},
        find: function(str) {},
        clear: function() {
            list.splice(0, list.length);
        }
    }
}());

