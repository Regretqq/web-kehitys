var express = require('express');
var app = express();
let mysql = require('mysql');
const util = require('util');
let con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "olsodb",
    database: "example_db"
});
const query = util.promisify(con.query).bind(con);

(async () => {
    try{
        const rows = await query('SELECT * FROM event');
        app.get('/', function (req, res) {
            res.send(rows);
        })
        }finally {
        con.end();
        }

})()


var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
})